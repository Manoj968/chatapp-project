from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import *

router = DefaultRouter()
router.register(r'v1/user', UsersViewSet, basename='UsersViewSet')

urlpatterns = [
    path('', include(router.urls)),
]
