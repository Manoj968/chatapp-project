from django.shortcuts import render
import os
import requests
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from django.core.paginator import Paginator ,PageNotAnInteger ,EmptyPage
# Create your views here.

class UsersViewSet(viewsets.ViewSet):
    def list(self, request):
        URL = 'https://reqres.in/api/users?page={}'.format(request.GET.get('p'))
        response = requests.get(url=URL)
        if response.status_code == 200:
            data = response.json()
            # paginator = Paginator(data, 10)
            # page = request.GET.get('page',1)
            # total_pages = paginator.num_pages
            # try:
            #     data = paginator.page(page)
            # except PageNotAnInteger:
            #     data = paginator.page(1)
            # except EmptyPage:
            #     data = paginator.page(paginator.num_pages)
            # data_list = [{"id":data.id,"page":data.page} for data in data]
            return Response({'data':data,"total_pages":"total_pages","success":True},status=status.HTTP_200_OK)
        return Response({"message":"str(error)","success":False}, status=status.HTTP_400_BAD_REQUEST)

# 