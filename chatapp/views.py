from django.shortcuts import render, HttpResponse
import time
import threading
import asyncio
from channels.layers import get_channel_layer

async def index(request):
    print("index: " + str(threading.get_native_id()))
    # channel_layer = get_channel_layer()
    # await channel_layer.group_send("love", {
    #     'type': 'chat_message',
    #     'message': 'notification',
    # })
    return HttpResponse("Done")

# def index(request):
#     print(threading.get_native_id())
#     return render(request, 'chatapp/index.html')

def room(request, room_name):
    return render(request, 'chatapp/room.html', {
        'room_name': room_name
    })