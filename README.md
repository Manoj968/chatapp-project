## Question 1

Check this page https://my-json-server.typicode.com/typicode/demo
Here you will find two api’s one for posts and comments 
https://my-json-server.typicode.com/typicode/demo/posts
https://my-json-server.typicode.com/typicode/demo/comments

What you need to do is write a code which will read data from both these api’s and assign comment to its respective post and finally return an object (dict/json) which has the combined data of both posts/comments.


## Question2 

Check out this website https://reqres.in/
It has a an api to get a list of users https://reqres.in/api/users?page=2
This api is page wise, i.e there are a total 12 pages and u can get a list of users for each page by passing the page number.

You need to write a function/code which will go through all pages and find the total number of users.


## Install Redis Server in Ubuntu

sudo apt install redis-server

## restart the Redis service

sudo systemctl restart redis.service

## starting redis server

redis-server

## checking that the Redis service is running

sudo systemctl status redis

## check server is working on cli
redis-cli
    >$ ping #ans is Pong


## to exit from cli
exit

redis-cli ping

## More about Redis server

https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-20-04

## if you uing window redis server will not support for windows in case use redis lab

## login -> https://app.redislabs.com/#/login after login you create database and important is endpoints for communication

signup -> https://redis.com/try-free/

## Learning How to make a web socket for chat app

https://channels.readthedocs.io/en/stable/tutorial/part_1.html
https://www.youtube.com/watch?v=plKYBBzqNLc

## channels
python3 -m pip install channels_redis

## Channels in settings.py

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
            # if using redis lab online redis server provider
            "hosts": [('redis://:redislabpassword@endpoint')]
        },
    },
}